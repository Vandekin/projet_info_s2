#ifndef _GRAPHE
#define _GRAPHE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

/*Definition of graph structure*/

/*Edge, represents a connection between 2 nodes, formerly known as T_ARC*/
typedef struct{
	int destination;//id of the node
	double cost;
} T_EDGE;

/*List of edges, used to enumerate the connections in a node to other nodes, formerly known as L_ARC*/
typedef struct succ{
	T_EDGE val;
	struct succ *next;
} *L_EDGE;

/*Node: represents the stations, formerly known as T_SOMMET*/
typedef struct _node{
  int id;
	char *name;
	char *line;
	double longi, lat;
	int scanned; //useful for the algorithm: belong to the closedSet
  struct _node *father; //useful for the algorithm
	L_EDGE neighbour;
} T_NODE;

/*Graphe: an array of nodes with a size*/
typedef struct _graphe{
	int size;
	T_NODE* nodes;
} T_GRAPHE;
//in case of trouble with the allocation of an uninterrupted area of memory: use blocs of 100 nodes instead

/*Functions: Creation, deletion T_NODE*/
T_NODE init_node(T_NODE node);
int empty_node(T_NODE* node);
void display_node(T_NODE *node, int arc_display);

/*Functions: Creation, deletion L_EDGE*/
int empty_edge(L_EDGE edge);
L_EDGE add_edge(int destination, double cost, L_EDGE edge_list);

void display_graphe(T_GRAPHE* graphe, int arc_display);


#endif
