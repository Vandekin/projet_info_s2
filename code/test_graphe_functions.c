#include "graphe.h"

int main()
{
  T_NODE node1;
  node1.name = "node1";
  node1.longi = 1;
  node1.lat = 0;
  node1.neighbour = NULL;

  puts("Display without neighbour");
  display_node(node1, 0);
  puts("");

  puts("Adding one neighbour");
  int destination = 36;
  double cost = 999.9;
  node1.neighbour = add_edge(destination, cost, node1.neighbour);
  display_node(node1, 1);
  puts("");

  puts("Adding another one");
  node1.neighbour = add_edge(10, 42.42, node1.neighbour);
  display_node(node1, 1);
  puts("");

  exit(EXIT_SUCCESS);
}
