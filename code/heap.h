#ifndef _HEAP
#define _HEAP

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"
#include "list.h"

typedef struct h_node
{
	T_NODE *node;
	double cost;
}H_NODE, *HEAP;

//======== CHANGE T_NODE TO POINTER ==========
int empty_heap(HEAP h);
void construct_heap(HEAP *h, int nbnode, T_NODE *node, double cost);
void add_heap_node(HEAP h, T_NODE *node, double cost);
int get_node_index(HEAP h, int nbnode, T_NODE *node);
void delete_heap_node(HEAP h,int node_index);
void lower_heap(HEAP h, int nbnode, int node_index);
void add_heap(HEAP h, int node_index);
void heap_sort(HEAP h, int nbnodes);
#endif
