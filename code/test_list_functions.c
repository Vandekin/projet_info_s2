#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"
#include "graphe.h"

T_NODE* createNode(int id, char* name, char* line);

int main()
{
  puts("Test list functions");
  puts("");
  List test_list = create_list();

  T_NODE *node1, *node2, *node3, *node4, *node5;
  node1 = createNode(1, "NODE 1", "L1");
  node2 = createNode(2, "NODE 2", "L1");
  node3 = createNode(3, "NODE 3", "L1");
  node4 = createNode(4, "NODE 4", "L1");
  node5 = createNode(5, "NODE 5", "L1");

  T_EDGE edge1, edge2, edge3;
  edge1.destination = 1;
  edge1.cost = 1;
  edge2.destination = 2;
  edge2.cost = 2;
  edge3.destination = 3;
  edge3.cost = 3;

  puts("Add node 1 to head");
  test_list = add_head(node1, test_list);
  display_list(test_list);
  puts("");

  puts("Add node 2 to head");
  test_list = add_head(node2, test_list);
  display_list(test_list);
  puts("");

  puts("Add node 3 to queue");
  test_list = add_queue(node3, test_list);
  display_list(test_list);
  puts("");

  List queue_list = create_list();
  puts("Get queue of test_list");
  queue_list = get_queue(test_list);
  display_list(queue_list);
  puts("");

  List new_list = create_list();
  puts("Fill a new list");
  new_list = add_head(node1, new_list);
  new_list = add_head(node2, new_list);
  new_list = add_head(node3, new_list);
  new_list = add_head(node4, new_list);
  display_list(new_list);
  puts("");

  List copied_list = create_list();
  puts("Copy the first list in a new one");
  copied_list = copy_list(test_list);
  display_list(copied_list);
  puts("");

  puts("Delete the head of the first list");
  test_list = remove_head(test_list);
  display_list(test_list);
  puts("");

  int real_size = size_list(copied_list) + size_list(new_list);

  puts("Concatenate the copied list with the new one");
  copied_list = concat_list(copied_list, new_list);
  display_list(copied_list);
  puts("");

  puts("Get the size of the concatenate list");
  int concatenate_list_size = size_list(copied_list);
  printf("Concatenate list size : %d\nReal size : %d\n", concatenate_list_size, real_size);
  puts("");

  puts("Test of the search function");
  List node_searched = search_list(node2, new_list);
  display_list(node_searched);
  puts("");

  puts("Delete all lists");
  delete_list(test_list);
  puts("First list deleted");
  delete_list(copied_list);
  puts("Second list deleted");

  puts("==================== ADD ORDER ====================");
  List order_list = create_list();
  puts("Creating a list ordered");
  puts("Add node 1, cost 1");
  order_list = add_in_order(node1, 1.0, order_list);
  puts("Add node 2, cost 2");
  order_list = add_in_order(node2, 2.0, order_list);
  puts("Add node 3, cost 3");
  order_list = add_in_order(node3, 3.0, order_list);
  puts("Add node 4, cost 4");
  order_list = add_in_order(node4, 4.0, order_list);
  display_list(order_list);

  puts("Add node 5, cost 2.5");
  order_list = add_in_order(node5, 1.5, order_list);

  puts("\nNew version:");
  display_list(order_list);

  puts("Delete node 5:");
  order_list = remove_node(node5, order_list);
  display_list(order_list);

  exit(EXIT_SUCCESS);
  return 0;
}

T_NODE* createNode(int id, char* name, char* line){
  T_NODE *node = calloc(1, sizeof(*node));
  if(node == NULL)
    return NULL;
  node->id = id;
  node->name = calloc(128, sizeof(*(node->name)));
  node->name = strcpy(node->name ,name);
  node->line = calloc(128, sizeof(*(node->line)));
  node->line = strcpy(node->line, line);
  node->longi = 0;
  node->lat = 0;
  node->scanned = 0;
  node->father = NULL;
  node->neighbour = NULL;
  return node;
}
