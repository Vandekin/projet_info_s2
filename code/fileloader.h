#ifndef _FILELOADER
#define _FILELOADER

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"
#include "list.h"

#define MAX_SIZE_NAME 256
#define MAX_SIZE_LINE 56

/*create a graphe from a file, allocates all the struct needed
	@param gr: @ of the pointer to put the graphe
	return: a pointer to the graphe
*/
T_GRAPHE* loadFromFile(char* fileName);

void debugDisplayNode(T_NODE *node);

#endif
