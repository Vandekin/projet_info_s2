#ifndef _ASTAR
#define _ASTAR

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "graphe.h"
#include "list.h"
//#include "heap.h"

double __heuristic_cost__(T_NODE *start, T_NODE *goal);
//double __cost__(T_NODE *current, T_NODE *next);
void __initialisation__(int nb_nodes, double *g, double *f, T_NODE *start, T_NODE *goal);
List __reconstruct_path__(T_GRAPHE *graphe, int start_id, int goal_id);
List a_star(int nb_nodes, T_NODE *start, T_NODE *goal, T_GRAPHE *graphe);

#endif
