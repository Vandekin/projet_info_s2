#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"
#include "list.h"
//#include "heap.h"
#include "fileloader.h"
#include "astar.h"

int main(int argc, char **argv){

	//char fileName[256];// = "../data/graphe1New.txt";
	//*/
	char *fileName;
	if(argc != 2){
		printf("Error: arguments\n");
		return 1;
	}
	fileName = argv[1];
	//*/

	printf("Loading of %s\n", fileName);
	T_GRAPHE *graphe = loadFromFile(fileName);
	if(graphe == NULL){
		printf("Error: impossible to load %s\n", fileName);
		return 1;
	}
	display_graphe(graphe, 1);

	int startId, goalId, displayDest;
	do{
		printf("Select the node id (between 0 and %d)\n", graphe->size);
		printf("Start id:\n");
		scanf("%d", &startId);
		printf("Goal id:\n");
		scanf("%d", &goalId);
	}while(startId<0 || startId>graphe->size || goalId<0 || goalId>graphe->size );

	T_NODE *start = &(graphe->nodes[startId]);
	T_NODE *goal = &(graphe->nodes[goalId]);

	printf("----------------------------\n");
	printf("Path from [%d]%s to [%d]%s\n", start->id, start->name, goal->id, goal->name);

	List path = a_star(graphe->size, start, goal, graphe);
	if(path != NULL){
		printf("Path proposed:\n");
		display_list(path);
	} else
		printf("Error: path null\n");

	exit(EXIT_SUCCESS);
}
