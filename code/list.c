#include "list.h"

/*Creation ,test and display fuctions*/
List create_list()
{
  return NULL;
}

List delete_list(List l)
{
  while(!empty_list(l))
  {
    l = remove_head(l);
  }
  return l;
}

int empty_list(List l)
{
  return l == NULL;
}

int size_list(List l)
{
  int size = 0;
  List current_list = l;
  for(current_list; !empty_list(current_list); current_list = current_list->next)
  {
    ++size;
  }
  return size;
}

void display_list(List l)
{
  List new_list = l;
  for(new_list; !empty_list(new_list); new_list = new_list->next)
  {
    display_node(new_list->node,1);
    puts("");
  }
}

/*Add. remove, copy, concatenation functions*/
List add_head(T_NODE *node, List l)
{
  List new_list = l;
  if((new_list = calloc(1, sizeof(*new_list))) != NULL)
  {
    new_list->node = node;
    new_list->next = l;
  }
  return new_list;
}

List add_queue(T_NODE *node, List l)
{
  List current_list = l, new_list = NULL, previous_list = NULL;
  if (!empty_list(current_list))
  {
    while(!empty_list(current_list))
    {
      previous_list = current_list;
      current_list = current_list->next;
    }
    if((new_list = calloc(1, sizeof(*new_list))) == NULL)
    {
      return NULL;
    }
    new_list->node = node;
    new_list->next = NULL;
    previous_list->next = new_list;
  }else
  {
    l = add_head(node, l);
  }
  return l;
}

//add the node to the list, take in account the cost,
//the lowest is on the head
List add_in_order(T_NODE *node, double cost, List l){
  //allocate the new cell
  List newList = calloc(1, sizeof(*newList));
  if(newList == NULL){
    printf("[List.c][add_in_order] Error: allocation\n");
    return l;
  }
  newList->node = node;
  newList->cost = cost;

  //security check: if 0 cells
  if(l == NULL){
    return newList;
  }

  //add to the right position
  //check if the cost is lower than the next one
  List previousList = NULL;
  List pList = l;//to navigate in the list
  while(pList != NULL){
    //if cheaper than the next one: add before
    if(newList->cost <= pList->cost){
      //case nothing before (1 cell in the list)
      if(previousList == NULL){
        newList->next = pList;//add to the head
        return newList;
      }

      //normal case: bewteen 2 cells
      newList->next = pList;
      previousList->next = newList;
      break;

    }

    //else next one
    previousList = pList;
    pList = pList->next;

    //security check: if we reached the end: add here
    if(pList == NULL){
      previousList->next = newList;
      newList->next = NULL;
      break;
    }
  }

  return l;
}

List remove_head(List l)
{
  if(!empty_list(l))
  {
    List old_cell = l;
    l = l->next;
    free(old_cell);
  }
  return l;
}

//A DEBUG
List remove_n(int n, List l)
{
  if(n == 0)
  {
    printf("[remove_n]ERROR : index smaller than 1\n");
    return l;
  }else if(n > size_list(l))
  {
    printf("[remove_n]ERROR : index greater than list size\n");
    return l;
  }else if(n == 1)
  {
    l = remove_head(l);
  }else
  {
    List current_list = l, previous_list = NULL;
    int position = 0;
    for(position; position < n-2; ++position)
    {
      current_list = current_list->next;
    }
    previous_list = current_list;
    current_list = current_list->next;
    current_list = remove_head(current_list);
    previous_list->next = current_list;
  }
  return l;
}

List remove_node(T_NODE *node, List l){

  List newList = l;

  List previousList = NULL;
  List pList = l;//to navigate in the list
  while(pList != NULL){
    //if it matchs
    if( (!strcmp(node->name, pList->node->name)) && (!strcmp(node->line, pList->node->line)) ) {
      if(previousList == NULL){//if the first one
        newList = pList->next;
        free(pList);
        break;//return the next
      }

      //normal case
      previousList->next = pList->next;
      free(pList);
      break;
    }

    //next
    previousList = pList;
    pList = pList->next;

    //if we reached the end: not in that copy_list
    if(pList == NULL){
      break;
    }
  }
  return newList;
}

List copy_list(List l)
{
  if(empty_list(l))
  {
    return NULL;
  }
  List new_list = create_list();
  while(!empty_list(l))
  {
    new_list = add_queue(l->node, new_list);
    l = l->next;
  }
  return new_list;
}

List concat_list(List l1, List l2)
{
  if(empty_list(l1))
  {
    return l1;
  }
  List current_list = l1, previous_list = create_list();
  while (!empty_list(current_list))
  {
    previous_list = current_list;
    current_list = current_list->next;
  }
  previous_list->next = l2;
  return l1;
}

/*Getter*/
List get_queue(List l)
{
  List list_queue = l;
  if(!empty_list(list_queue))
    {
      while(list_queue->next != NULL)
        {
          list_queue = list_queue->next;
        }
    }
  return list_queue;
}


/*Search functions*/
List search_list(T_NODE *node, List l)
{
  List current_list = l;
  while(!empty_list(current_list))
  {
    if(current_list->node->name == node->name)
    {
      return current_list;
    }
    current_list = current_list->next;
  }
  printf("[search_list]ERROR : node not found\n");
  return NULL;
}

int search_list_index(T_NODE *node, List l){
  List current_list = l;
  int index = 0;
  while(!empty_list(current_list))
  {
    if(current_list->node->name == node->name)
    {
      return index;
    }
    current_list = current_list->next;
    index++;
  }
  printf("[search_list_index]ERROR : node not found\n");
  return -1;
}
