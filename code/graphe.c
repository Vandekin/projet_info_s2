#include "graphe.h"

#define NAME_SIZE 512

/*Node function*/
T_NODE init_node()
{
  /*
    T_NODE node;
    node.name = NULL;
    node.longi = 0.0;
    node.lat = 0.0;
    node.scanned = 0;
    node.father = NULL;
    node.neighbour = NULL;
    return node;
  */

}

int empty_node(T_NODE* node)
{
  return (node == NULL);
  //return (node->name == NULL);
}

void display_node(T_NODE *node, int arc_display){

  if(!empty_node(node)){
      L_EDGE p;
      printf("\tLine: %s\n", node->line);
      printf("[%d] Name : %s\n\tLongitude : %.2lf\tLatitude : %.2lf\n", node->id, node->name, node->longi, node->lat);
      if(arc_display) {
        p = node->neighbour;
          while(p != NULL){
              printf("\t\tDestination : %i\tCost : %.2lf\n", p->val.destination, p->val.cost);
              p = p->next;
            }
        }
    }else{
      printf("Empty node\n");
    }
}

/*Edge functions*/
int empty_edge(L_EDGE edge)
{
  return edge == NULL;
}

T_EDGE create_edge(int destination, double cost) //use by add_head
{
  T_EDGE edge;
  edge.destination = destination;
  edge.cost = cost;
  return edge;
}

L_EDGE add_edge(int destination, double cost, L_EDGE edge_list)
{
  L_EDGE new_edge_cell = NULL;
  if((new_edge_cell = calloc(1, sizeof(*new_edge_cell))) == NULL)
    {
      printf("[add_edge]ERROR : not enough memory");
      return NULL;
    }
  new_edge_cell->val = create_edge(destination, cost);
  new_edge_cell->next = NULL;
  if(empty_edge(edge_list))
    {
      edge_list= new_edge_cell;
    }else
    {
      L_EDGE current_edge_cell = edge_list, previous_edge_cell = NULL;
      while(!empty_edge(current_edge_cell))
        {
          previous_edge_cell = current_edge_cell;
          current_edge_cell = current_edge_cell->next;
        }
      previous_edge_cell->next = new_edge_cell;
    }
  return edge_list;
}

void display_graphe(T_GRAPHE* graphe, int arc_display){
  printf("====================GRAPHE====================\nSize: %d\n", graphe->size);
  int i;
  for(i=0; i<graphe->size; i++){
    display_node(&(graphe->nodes[i]), arc_display);
  }
}
