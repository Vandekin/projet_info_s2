#ifndef _LIST
#define _LIST

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"

typedef struct _list {
  T_NODE *node;
  double cost;
  struct _list *next;
} *List;


/*Creation, deletion, test and display fuctions*/
List create_list();
List delete_list(List l);
int empty_list(List l);
int size_list(List l);
void display_list(List l);

/*Add. remove, copy, concatenation functions*/
List add_head(T_NODE *node, List l);
List add_queue(T_NODE *node, List l);
List add_in_order(T_NODE *node, double cost, List l);
List remove_head(List l);
List remove_n(int n, List l);//A DEBUG
List remove_node(T_NODE *node, List l);
List copy_list(List l);
List concat_list(List l1, List l2);

/*Getter*/
List get_queue(List l);

/*Search functions*/
List search_list(T_NODE *node, List l);
int search_list_index(T_NODE *node, List l);

#endif
