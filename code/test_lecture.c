#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "graphe.h"

int main(int argc, char **argv)
{
	//Verification of the input file
	if(argc != 2)
	{
		perror("Not enought argument :\n./main name_of_the_file\n");
		return 1;
	}


	//Open the input file
	FILE *data = fopen(argv[1], "rt");
	if(data == NULL)
	{
		printf("Open %s failed", argv[1]);
		return 1;
	}
	

	//Read the input file
	int nedges, nnodes, ibuf, k=0;
	double fbuf;
	char buffer[512], cbuf;
	/*Get the number of nodes and edges of the graph*/
	fscanf(data, "%d %d", &nnodes, &nedges);
	//printf("%d\t%d\n", nnodes, nedges);
	
	fgets(buffer, sizeof(buffer), data);
	
	for(int i = 0; i < nnodes; ++i)
	{
		fscanf(data, "%d", &ibuf);
		fscanf(data, "%lf", &fbuf);
		//printf("%lf", fbuf);
		fscanf(data, "%lf", &fbuf);
		//printf("%lf", fbuf);
		fscanf(data, "%s", buffer);
		fgets(buffer, sizeof(buffer), data);
		memset(buffer, '\0', sizeof(buffer));
		while(k<511 || cbuf == '\n')
		{
			cbuf = fgetc(data);
			if(cbuf != '\t')
			{
				//buffer[k] = cbuf;
				strcpy(&buffer[k], &cbuf);
				++k;
			}
			printf("%d\n", k);
		}
		printf("%s\n", buffer);
	}
	
	//Close the input file
	if(fclose(data) != 0)
	{
		printf("Error closing %s", argv[1]);
		return 1;
	}
	
	return 0;
}


