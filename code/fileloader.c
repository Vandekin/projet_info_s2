#include "fileloader.h"

/*create a graphe from a file, allocates all the struct needed
	@param gr: @ of the pointer to put the graphe
	return: a pointer to the graphe
*/
T_GRAPHE* loadFromFile(char* fileName){

	//file openning and security check
	if(fileName == NULL){
		printf("[loadFromFile] Error: fileName null\n");
		return NULL;
	}
	FILE *file = fopen(fileName, "rt");
	if(file == NULL){
		printf("[loadFromFile] Error: impossible to open %s\n", fileName);
		return NULL;
	}

	//params and buffers
	int nodeNb, edgeNb, intBuffer;
	char strBuffer[MAX_SIZE_NAME];

	//get the graphe param
	fscanf(file, "%d %d ", &nodeNb, &edgeNb);
	printf("Param loaded: nodeNb: %d\t edgeNb: %d\n", nodeNb, edgeNb);

	//skip the useless line
	fgets(strBuffer, MAX_SIZE_NAME-1, file);

	//allocation of the graphe/nodes
	T_GRAPHE* gr = calloc(1, sizeof(*gr));
	if(gr == NULL){
		printf("[loadFromFile] Error: graphe allocation\n");
		return NULL;
	}
	gr->size = nodeNb;
	//debug test
	//printf("%d %d\n",sizeof(gr->nodes),sizeof(T_NODE));
	gr->nodes = calloc(nodeNb, sizeof(*gr->nodes));
	if(gr->nodes == NULL){
		printf("[loadFromFile] Error: nodes allocation\n");
		free(gr);
		return NULL;
	}

	//read all the nodes
	int nCount;
	for(nCount = 0; nCount<nodeNb; nCount++){
		//allocate the str
		gr->nodes[nCount].line = calloc(MAX_SIZE_LINE, sizeof(*gr->nodes[nCount].line));
		gr->nodes[nCount].name = calloc(MAX_SIZE_NAME, sizeof(*gr->nodes[nCount].name));

		gr->nodes[nCount].id = nCount;
		fscanf(file, "%d %lf %lf %s",//retrieve id, longi, lat, ligne
			&intBuffer, &(gr->nodes[nCount].lat), &(gr->nodes[nCount].longi), gr->nodes[nCount].line);
		fgets(gr->nodes[nCount].name, MAX_SIZE_NAME-1, file);//retrieve name

		//check the name
		//if(gr->nodes[nCount].name[MAX_SIZE_NAME-1]<32)//get rid of special char
		//	gr->nodes[nCount].name[MAX_SIZE_NAME-1] = 0;
		int i;
		for(i=MAX_SIZE_NAME-2; i>=0; i--){//start at the penultimate
			if(gr->nodes[nCount].name[i]>32){//if a true char is found, put \o after and quit
				gr->nodes[nCount].name[i+1]='\0';
				break;
			}
		}

		//initialise the other fields
		gr->nodes[nCount].father = NULL;
		gr->nodes[nCount].scanned = 0;
		gr->nodes[nCount].neighbour = NULL;
		//debugDisplayNode(&(gr->nodes[nCount]));
	}

	//skip the useless line after the nodes
	fgets(strBuffer, MAX_SIZE_NAME-1, file);

	//read and add the edges to the graphe's nodes
	//buffers
	int start, destination;
	double cost;

	int eCount;
	for(eCount=0; eCount<edgeNb; eCount++){
		fscanf(file, "%d %d %lf", &start, &destination, &cost);//start destination cost
		//printf("start: %d dest: %d cost %lf\n", start, destination, cost);//debug
		gr->nodes[start].neighbour = add_edge(destination, cost, gr->nodes[start].neighbour);
	}

	fclose(file);
	return gr;
}

void debugDisplayNode(T_NODE *node){
	printf("[%d] Name: %s\tLine: %s\n\tL: %lf\tLat: %lf\n", node->id, node->name, node->line, node->longi, node->lat);
}
