#include "astar.h"

double __heuristic_cost__(T_NODE *start, T_NODE *goal)
{
  return ((fabs(start->longi - goal->longi) + fabs(start->lat - goal->lat))/2);
}

// double __cost__(T_NODE *current, T_NODE *next)
// {
// }


//initialise the array g, f. The graph is supposed to be already init during the loading
void __initialisation__(int nb_nodes, double *g, double *f, T_NODE *start, T_NODE *goal)
{
  if((g = calloc(nb_nodes, sizeof(*g))) == NULL)
    {
      printf("[__initislisation__]ERROR : not enough memery");
      return;
    }
  if((f = calloc(nb_nodes, sizeof(*f))) == NULL)
    {
      printf("[__initialisation__]ERROR : not enough memory");
      return;
    }
  for(int i = 0; i < nb_nodes; ++i)
    {
      g[i] = INFINITY;
      f[i] = INFINITY;
    }
  g[0] = 0;
  f[0] = __heuristic_cost__(start, goal);
  return;
}

List __reconstruct_path__(T_GRAPHE *graphe, int start_id, int goal_id)
{
  List path = NULL;
  if((path = calloc(1, sizeof(*path))) == NULL)
    {
      printf("[__reconstruct_path__]ERROR : not enough memory");
      return NULL;
    }
  int current_id = goal_id;
  do
    {
      path = add_head(graphe->nodes[current_id], path);
      if(current_id == start_id)
        {
          break;
        }
      current_id = graphe->nodes[current_id].father->id;
    }while(current_id != start_id);
  return path;
}

List a_star(int nb_nodes, T_NODE* start, T_NODE* goal, T_GRAPHE *graphe)
{
  double *g = NULL, *f = NULL; //access cost: f: total(estimated), g: const from start
  HEAP openSet = NULL; //nodes not visited yet, accessible from the visited nodes, cost in f
  //the closedSet (nodes already visited): node.scanned, cost in g
  int const heapSize = nb_nodes;

  //init the cost array, the graphe is already init during loading
  __initialisation__(nb_nodes, g, f, start, goal);

  int currentNodeId = 0;
  while(openSet != NULL){
    //get the next node with the lowest estimated cost among the available
    currentNodeId = openSet->node->id;
    if(currentNodeId == goal->id){//if goal is reached
      return __reconstruct_path__(graphe, start->id, goal->id);
    }

    //switch currentNode from openSet to closedSet
    delete_heap_node(openSet, get_node_index(openSet, heapSize, &(graphe->nodes[currentNodeId]) ));
    graphe->nodes[currentNodeId].scanned = 1;

    double costToNeighbour, costGTest, costFTest;
    T_NODE* neighbourNode;
    //for each node accessible from currentNode
    L_EDGE edgeListCell = graphe->nodes[currentNodeId].neighbour;
    while(!empty_edge(edgeListCell)){
      neighbourNode = &(graphe->nodes[edgeListCell->val.destination]);
      costToNeighbour = edgeListCell->val.cost;

      if(neighbourNode->scanned)//ignore the nodes already visited
        continue;

      costGTest = g[currentNodeId] + costToNeighbour;

      if(neighbourNode->scanned == 0){//if it's an unknown node, add it to the visited one
        costFTest = costGTest + __heuristic_cost__(neighbourNode, goal);
        add_heap_node(openSet, neighbourNode, costFTest);
        neighbourNode->scanned = 1;
        //f and g are saved later
      }else if(costGTest < g[neighbourNode->id]){//if already known: test is it's faster way to go there
        //if it's a better path: update the openSet sorting
        costFTest = costGTest + __heuristic_cost__(neighbourNode, goal);
        delete_heap_node(openSet, get_node_index(openSet, heapSize, neighbourNode));
        add_heap_node(openSet, neighbourNode, costFTest);
      }else{
        continue;//if the cost is higher than the previous, skip that path
      }

      //record the path found and costs
      neighbourNode->father = &(graphe->nodes[currentNodeId]);
      g[neighbourNode->id] = costGTest;
      f[neighbourNode->id] = costGTest + __heuristic_cost__(neighbourNode, goal);

      edgeListCell = edgeListCell->next;
    };

  };

  //if we got there: the goal and the start node are not linked
  printf("Goal is not reachable from Start\n");
  return NULL;
}
