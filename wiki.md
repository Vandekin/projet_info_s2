# Wiki code projet
Fichier utile pour la prise de note au sujet de structure importantes et de méthodologie.

## Organisation des données:
* Graphe: tableau de structre noeud (T_NODE)
* Liste ouverte (Noeuds atteinds): Tas, lié à G(node): cout depuis départ
* Liste fermée (Cout pour atteindre chaque noeud): Champs dans la structure
* Liste des peres (Noeud précédent): Champs ds le noeud


Backup pour Graphe si tableau 1 seul contigues impossible: tableau de tableau de 100 structures chacun, besoin d'une fct qui permet l'acces au structures de manière transparente


## Decoupe du code:
A faire:
* Structures et fonctions associées: tas et liste (fct déjà dev)
    * Graphe: getNode(int id) (fichier graphe)
    * Liste ouverte: isScanned() (fichier graphe)
    * Liste fermée: getCost(), setCost(int c) (fichier tas)
    * Liste père: getFatherId(int id), getFather(int id) (fichier graphe)
* Visualisation structures et listes
* Chargement depuis fichier
* Algo (redécouper en fct)
    * Fct initialisation
    * Fct G(s): calcul cout chemin determiné et H(s): estimation cout chemin restant
    * buildPath(): construit une liste chainée avec le chemin calculé
* Bonus: Représentation graphique

En terme de fichiers:
* Main.c
* Graphe.c/.h
* Liste.c/.h
* Tas.c/.h
* FileLoader.c/.h
* AStar.c./.h
* Bonus: Diplay.c/.h

## Algo commenté
```
**Initialisation**
G(d) <- 0
Calculer H(d,a)
F(d) <- G(d) + H(d,a)
**for** tous les autres sommets k **do**
F(k) <- G(k) <- H(k,a) <- inf
**end for**
LO <- {d}; LF <- /0
k <- d
```
